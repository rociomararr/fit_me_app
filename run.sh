#!/bin/bash
. ~/.nvm/nvm.sh
. ~/.profile
. ~/.bashrc

nvm i
nvm use
echo Installing dependencies
npm i

echo "Cleaning dist directory"
rm -rf dist
echo "Building new app"

npm run dev