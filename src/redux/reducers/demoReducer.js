import { initial, commonReducer } from './commonReducers';

export function test (state = initial, action) {
    return commonReducer(state, action, 'test');
}