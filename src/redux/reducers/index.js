import { combineReducers } from 'redux';

import * as demoReducers from './demoReducer';


const state = combineReducers({
    ...demoReducers
});

export default state;
