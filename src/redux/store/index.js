// DEVELOPMENT
import {createStore, applyMiddleware} from 'redux';
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';

import rootReducer from '../reducers';
const isProduction = process.env.env === 'production';

let configureStore;
if (!isProduction) {
    const enhancer = composeWithDevTools(
        applyMiddleware(thunk, createLogger())
    );
    
    configureStore = function configureStore (initialState) {
        return createStore(rootReducer, initialState, enhancer);
    };
} else {
    configureStore = function configureStore (initialState) {
        return createStore(rootReducer, initialState, applyMiddleware(thunk));
    };
}

export default configureStore;