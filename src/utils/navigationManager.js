import history from './history';

export const goToUrl = (url) => {
    history.push(url);

};

export const goToHome = () => {
    const URL = '/';
    history.push(URL);
};