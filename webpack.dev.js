const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const webpack = require('webpack');

const commonRules = require('./webpack.common');

const APP_DIR = path.resolve(__dirname, './src/');
const entryJS = path.resolve(APP_DIR, 'index.js');
const entryCss = path.resolve(APP_DIR, 'sass/index.scss');

module.exports = {
    mode: 'development',
    entry: [entryJS, entryCss],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.[hash].js',
        publicPath: '/'
    },
    module: {
        rules: [
            ...commonRules,
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader // creates style nodes from JS strings
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            minimize: true
                        } // translates CSS into CommonJS
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: false
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        modules: [APP_DIR, 'node_modules'],
        extensions: ['.js', '.json', '.jsx', '.css'],
    },
    performance: {
        maxEntrypointSize: 400000, // int (in bytes)
        assetFilter: function(assetFilename) {
            // Function predicate that provides asset filenames
            return assetFilename.endsWith('.css') || assetFilename.endsWith('.js');
        }
    },
    target: 'web',
    devtool: 'source-map',
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 3000,
        hot: true,
        historyApiFallback: true,
        open: true
    },
    plugins: [
        new webpack.EnvironmentPlugin({
            ...require('./env.json')
        }),
        new MiniCssExtractPlugin({
            filename: 'bundle.[hash].css',
        }),
        new HtmlWebpackPlugin({
            title: 'Harbingers of devastation',
            template: path.join(__dirname, 'src', 'index.html'),
            filename: 'index.html',
        })
    ]
};