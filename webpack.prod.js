const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const webpack = require('webpack');

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const commonRules = require('./webpack.common');

const APP_DIR = path.resolve(__dirname, './src/');
const entryJS = path.resolve(APP_DIR, 'index.js');
const entryCss = path.resolve(APP_DIR, 'sass/index.scss');

console.log(process.env.env);
module.exports = {
    mode: 'production',
    entry: [entryJS, entryCss],
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.[hash].js'
    },
    module: {
        rules: [
            ...commonRules,
            { // sass / scss loader for webpack
                test: /\.(sass|scss)$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader'],
            },
        ]
    },
    resolve: {
        modules: [APP_DIR, 'node_modules'],
        extensions: ['.js', '.json', '.jsx', '.css', '.*css'],
    },
    performance: {
        hints: 'warning', // enum    maxAssetSize: 200000, // int (in bytes),
        maxEntrypointSize: 400000, // int (in bytes)
        assetFilter: function(assetFilename) {
            // Function predicate that provides asset filenames
            return assetFilename.endsWith('.css') || assetFilename.endsWith('.js');
        }
    },
    target: 'web',
    plugins: [
        new webpack.EnvironmentPlugin({
            ...require('./env.json')
        }),
        new MiniCssExtractPlugin({
            filename: 'bundle.[hash].css',
        }),
        new HtmlWebpackPlugin({
            title: 'Fit me app',
            template: path.join(__dirname, 'src', 'index.html'),
            filename: 'index.html'
        }),
        new UglifyJsPlugin({
            
        })
    ]
};